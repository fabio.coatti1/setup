Feature: assessment_scheduled

    As a compliance manager
    I want to assess rules related to an asset set on a regular period e.g. weekly
    so that:
    - compliance states and violations are uptodate in case of
    - changing rules without asset changes
    - asset changes missed by the real-time flow
    - cost can be optimized by aligning the request timeframe to the scheduled period

    Scenario: Get compliance results and pass SLOs when triggering the general analysis
        Given the ram project is your-ram-project-id
        And the actions.json file has been copied to the actionsrepo gcs bucket
        When the job at_00am00_on_day_of_month_1_in_january located in europe-west1 is manually triggered
        And waiting for the latency threshold of 01-ram service ram-e2e-latency-scheduled-general slo
        And waiting for 360 seconds to ingest logbased metrics
        Then there is no asset in the scope with an update time older than the job starttime
        And filtering on the triggering job there are assets the count of compliance status equal the count of assets for each rule deployed on a given asset type excluding IAM rules
        And filtering on the triggering job there is no violation with a related compliance status as compliant
        And filtering on the triggering job there is no not compliant status without a violation to explain why
        And filtering on the triggering job there is only one last compliance status per rule per asset
        And filtering on the triggering job there is no violation with a null update time
        And the burnrate is smaller than 1 for ram-e2e-latency-scheduled-general slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-convertfeed-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-execute-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-fetchrules-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-launch-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-monitor-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-publish2fs-availability slo on a loopback period aligned to the test
        And the burnrate is smaller than 1 for ram-microservice-stream2bq-availability slo on a loopback period aligned to the test
