# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
package templates.gcp.GCPCloudfunctionLocationConstraintV1
import future.keywords.in

import data.validator.gcp.lib as lib

deny[{
	"msg": message,
	"details": metadata,
}] {
	constraint := input.constraint
	lib.get_constraint_params(constraint, params)
	mode := lib.get_default(params, "mode", "allowlist") 
	target_locations := lib.get_default(params, "locations", ["EU"])

	asset := input.asset

	# Applies to GCF only
	asset.asset_type == "cloudfunctions.googleapis.com/CloudFunction"

    # Retrieve the location
    asset_location := asset.resource.location

    # The asset raises a violation if check_location is evaluated to false
 	check_location(mode,asset_location,target_locations)

	message := sprintf("CloudFunction %v is in a disallowed location (%v).", [asset.resource.data.name, asset_location])
	metadata := {"resource": asset.name}
}

#################
# Rule Utilities
#################

# Determine the overlap between locations under test and constraint
# By default (allowlist), we violate if there isn't overlap
#
check_location(mode,asset_location,target_locations) {
	mode == "allowlist"
	not asset_location in target_locations
}
check_location(mode,asset_location,target_locations) {
	mode == "denylist"
	asset_location in target_locations
}